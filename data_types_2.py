a =10
b = "Hello There"
c = ["Red","Green","blue"] #mutable
d = 10000.00
e = 10+5j
f = (10001,"Peter Parker",10002) #immutable
g = {"Name":"Aman","r_no":1,"subjects":["Python","Computer Graphis"]}
h = {30,10,20,30} #Set contains unordered and unique collection of elements
i = set()
j = None
k = False

print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))
print(type(h))
print(type(i))
print(type(h))
print(type(k))