def student(**kwargs):
    # print(kwargs)
    for k,v in kwargs.items():
        print("{}: {}".format(k,v))
    print("-------------------------")

student(name="Amandeep Kaur")
student(first_name="Peter",last_name="Parker",roll_no=1)
student(email="Peter@gmail.com",roll_no=1)