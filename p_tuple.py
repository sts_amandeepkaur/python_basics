ab = 10,20,30

print(type(ab))
print(ab)

### Indexing
print(ab[1])
print(ab[-1])
print(len(ab))

col= ("Red","Green","Orange","Pink","magenta")

#Tuple contains immutable collection of elements

print(type(col))

# del col[0] ## Tuple does not support item deletion
print(col)

### Slicing
print(col[2:])
print(col[:2])
print(col[1:4])

### Stepsize
print(col[::1])
print(col[::2])
print(col[::-2])