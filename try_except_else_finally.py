try:
    name = "Amandeep Kaur"
    print(name)
    # print(name+10)

except:
    print("EXCEPT BLOCK!!!")

else:
    print("ELSE BLOCK: I will execute if no error in try block :)")

finally:
    print("FINALLY! I will execute ALWAYS :)")