class Student:
    clz="ABCD ENGG> COLLEGE"
    total_fee=100000
    __acc=10000000005432
    def __init__(self):
        print("Parent Const. ")

    def intro(self):
        print("Student of: ",self.clz)

class Account(Student):
    clz = "XYZ College of IT"

    def __init__(self):
        super().__init__()
        # print("ACCOUNT NO.",self.__acc) ## Child class can't access private Data members of a class
        self.intro()
        print("Child Const. Rs. {}/-".format(self.total_fee))

    def fee_detail(self):
        print("Fee Details: ")

s1 = Account()
# s1.fee_detail()
print(dir(s1))