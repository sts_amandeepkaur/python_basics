class Student:
    clz  = "XYZ Engg. College"
    def intro(self, name, roll_no):
        self.n = name
        self.r = roll_no
        print(self.n, "registred!!")
    def get_information(self):
        print("Name:{}\nRoll Number:{}".format(self.n,self.r))

## Single Inheritance
class Library(Student):
    def issue_book(self,bn,isdt,rtdt):
        print(self.n , "Book details:")
        print("Book Name:{} \nIssue Date:{} \nReturn Date:{}".format(bn,isdt,rtdt))


s1 = Library()
s2 = Library()
# print(s1.clz)
s1.intro("Aman",10001)
s2.intro("Peter",10002)

s1.get_information()
# s1.issue_book("Python Programming","11-May-2020","21-May-2020")
s1.issue_book("Operating System","11-May-2020","21-May-2020")