st1 = {10,20,10,30}
st2 = {100,10,15,20,20}

##UNION
# u = st1.union(st2)
u = st1|st2
print("union: ",u)

## INTERSECTION
# i = st1.intersection(st2)
i = st1&st2
print("intersection: ",i)

##Difference
# d = st1.difference(st2)
# d = st1-st2
d = st2-st1
print("diff: ",d)

