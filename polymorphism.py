# Function Overloading
# Function Overriding

class Arithmetic:
    def add(self,x,y):
        return x+y 

    def add(self,x,y,z):
        return x+y+z

obj = Arithmetic()
print(obj.add(5,7,2))
# print(obj.add(5,89)) #Python does not support function overloading