#Method Overloading
#Method Overriding

class MyClass:
    def add(self,x,y):
        return x+y

    def add(self,x,y,z):
        return x+y+z


obj = MyClass()
print(obj.add(10,30,29))
print(obj.add(10,30)) #Python doesn't support method overloaing

# print(5+7) #Arithmetic Operator
# print("5"+"7") #Concatenation Operator