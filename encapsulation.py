class Student:
    #Public Data Member
    clz = "Abcd Engg. College"

    #Private Data Member
    __pin = 10000875

    #Private Member Function
    def __intro(self):
        print("Member function of a class",self.__pin)

    def __init__(self):
        self.__intro()

class Child(Student):
    def __init__(self):
        print(self.clz)
        # print(self.__pin) ## Child class can't use private D.M. of parent class

s1 = Child()
s2 = Student()
# print(dir(s1))
print(isinstance(s1,Child))
print(isinstance(s2,Child))
# obj = Student()
# print(obj.__pin) ##We can't access private data members to outside the class
# print(obj.clz)
# obj.clz = "XYZ College of Management"
# print(obj.clz)
