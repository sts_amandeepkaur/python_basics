ab = [10,49,98,9,4,77,"hello",48]
print(ab)

print(type(ab))

#Indexing
print(ab[0])
print(ab[-1])
print(ab[-3])

total = len(ab)
print("last element: ",ab[total-1])
# Why List?
colors = ["red","green","blue","black","orange","pink","gray"]
col = ["magenta","yellow"]
## Slicing
print(colors[2:])
print(colors[:2])
print(colors[2:5])

## Step Size
print(colors[::1])
print(colors[::2])
print(colors[::3])

## Reverse a list using slicing
print(colors[::-1])

## Concatenate
print(ab+colors)
print(col+colors)

## Membership
print("magenta" in col)
print("green" not in col)

