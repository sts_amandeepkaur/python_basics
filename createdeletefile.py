import os
import requests

con = requests.get("https://restcountries.eu/rest/v2/all").json()
ab = ""
for i in con:
    ab+="<div style='text-align:center;width:20%;padding:1%;margin:1%;float:left;box-shadow:0px 0px 10px gray;'>"
    ab+="<h1 style='color:red;'>{}</h1>".format(i["name"])
    ab+="<p><em>{}</em></p>".format(i["population"])
    ab+="<p><em>{}</em></p>".format(i["borders"])
    ab+="<img src='{}' style='height:200px;width:100%;'>".format(i["flag"])
    ab+="</div>"

ab+="<hr/>"

try:
    f = open("countries.html","w")
    f.write(ab)
    f.close()
    print("File created Successfully!!!")

except FileExistsError:
    os.remove("countries.html")
    print("File deleted Successfully!!")