class Circle:
    pi = 3.14

    def set_radius(self,rad):
        self.r = rad
        print(rad,"Radius Set")

    def calc_area(self):
        print("Radius:{} Area:{}".format(self.r,self.pi*self.r**2))


c1 = Circle()
c2 = Circle()
c3 = Circle()

c1.set_radius(5)
c2.set_radius(10)
c3.set_radius(8)

# c1.calc_area()
c3.calc_area()
c2.calc_area()

print(c1.pi)