class Base1:
    a = 10
    def intro1(self):
        print("Hii It is Intro 1")

class Base2:
    def intro2(self):
        print("Hii It is Intro 2")

### Multiple Inheritance
class child(Base1,Base2):
    def hello(self):
        print("It is function inside child class")


# ob1 = Base1()
# ob1.intro1()

ob2 = child()
ob2.hello()
ob2.intro1()
print(ob2.a)
# print(dir(ob2))
ob2.intro2()

ob3 = Base2()
print(dir(ob3))