class Parent:
    a= "hello"

    def fun2(self):
        print("Member Function of Parent Class")

class Child(Parent):
    a = "There"

    def fun(self):
        print("a=",super().a)
        super().fun2()
        print("Member Function of child class")


obj = Child()
# print(obj.a)
obj.fun()
# obj.fun2()