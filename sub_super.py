ab = {10,20,40,50,30}
cd = {10,20,50}

# print(cd.issubset(ab))
if cd.issubset(ab):
    print("cd is subset of ab")
else:
    print("cd is not subset of ab")

# print(ab.issuperset(cd))
if ab.issuperset(cd):
    print("ab is superset of cd")
else:
    print("ab is not superset of cd")