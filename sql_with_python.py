import pymysql 

try:
    db =pymysql.connect(
        host="localhost",
        user="root",
        password="test",
        database="test_database"
    )

    def create_record():
        name = input("Enter Name: ")
        salary = int(input("Enter Salary: "))

        cur = db.cursor()
        cmd = "INSERT INTO employee(name,salary) VALUES('{}','{}')".format(name,salary)
        cur.execute(cmd)
        print(name,"Inserted Successfully")

    def fetch_data():
        cur = db.cursor()
        cmd="SELECT * FROM employee"
        cur.execute(cmd)

        for id,name,salary in cur.fetchall():
            print(id,name,salary)
    while True:
        ch = input("Enter Choice: ")
        if ch=="1":
            create_record()
        elif ch=="2":
            fetch_data()
        elif ch=="0":
            break
        else:
            print("Invalid Choice")
except:
    print("Could Not Connect!!!\nPlease Check Again!!!")