class Student:
    #Data Members
    clz = "Abcd Engineering College"

    #Member Function
    def myfun(self):
        print("I am a member function inside Student class")

    def details(self,name,roll_no):
        print("Name:{} Roll No: {} College:{}".format(name,roll_no,self.clz))


### Creating a object

obj = Student()
# print(obj.clz)

obj.myfun()
obj.details("Aman",1002)
obj.details("Peter",1003)

print(dir(Student))