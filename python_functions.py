## Functions - Buit In, User Defined

ab = "Python was created by Guido Van Rossum"
print(dir(ab))
print(ab.upper())
print(ab.lower())
print(ab.title())
print(ab.capitalize())
print(ab.swapcase())

cd,str = "PYTHON","there"
print(cd.isupper())
print(cd.islower())
print(str.islower())

nums = "9876598ukj"
print("nums=",nums.isdigit())
print(nums.isalpha())
print(nums.isalnum())

email= "aman@gmail.com"
s=email.split("@")
print(s[0])
print(s[1])

print(ab.split())
print(ab.split("y"))
