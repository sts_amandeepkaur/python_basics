lang = ("HTML","CSS","JS","Python","CSS")
ab = ("PHP","Android")

## Membership operator
if "Java" in lang:
    print("Element Found")
else:
    print("Element not fond")

## Concatenate
print(lang+ab)

print(dir(lang))

### Methods
print(lang.count("JQuery"))
print(lang.count("CSS"))

print(lang.index("CSS"))
print(lang.index("CSS",2))

num = 10,20,40,86,8
print(sum(num))
print(num.index(40))