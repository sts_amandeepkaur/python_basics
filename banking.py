import random

users = []
def create_account():
    name = input("Please Enter Your Name: ")
    initial = float(input("Enter Initial Amount: "))
    acc_no = random.randint(1000,9999)
    cust = {"name":name,"balance":initial,"acc_no":acc_no}
    users.append(cust)
    print("Dear {} your account created successfully, Acc. No.: {}".format(name,acc_no))

def deposit():
    acc= int(input("Enter Your Account Number: "))
    flag = False
    for i in users:
        if i["acc_no"] == acc:
            flag = True
            print("Welcome: ",i["name"])
            amt = float(input("Enter Amount to deposit: "))    
            i["balance"] += amt
            print("Your account credited with Rs.{}/- Current Balance: {} ".format(amt,i["balance"]))
    if flag==False:
        print("Incorrect Account Number ")

def withdraw():
    acc= int(input("Enter Your Account Number: "))
    flag = False
    for i in users:
        if i["acc_no"] == acc:
            flag = True
            print("Welcome: ",i["name"])
            amt = float(input("Enter Amount to withdraw: "))    
            if amt > i["balance"]:
                print("You dont have sufficient balance, Your Balance: Rs.{}/-".format(i["balance"]))
            else:
                i["balance"] -= amt
                print("Your account debited with Rs.{}/- Current Balance: {} ".format(amt,i["balance"]))
    if flag==False:
        print("Incorrect Account Number ")

def check_balance():
    acc = int(input("Enter Your Account Number: "))
    
    match = False
    for i in users:
        if i["acc_no"]==acc:
            match=True
            print("Welcome: ",i["name"])
            print("Your Balance: ",i["balance"])
    if match==False:
        print("Incorrect Account Number: ")

options = """
    **********  WELCOME TO OUR BANK **************
    Press 1: To create Account
    Press 2: To check balance
    Press 3: To Deposit
    Press 4: To withdraw
    Press 5: To exit
"""
print(options)
while True:
    choice = input("Enter Your Choice: ")
    if choice=="1":
        create_account()
    elif choice=="2":
        check_balance()
    elif choice=="3":
        deposit()
    elif choice=="4":
        withdraw()
    elif choice=="5":
        break
    else:
        print("Invalid Choice")
