grocery = ["Milk","Apple","Oil","Milk","Bread","Mustrad","Eggs","Milk","Flour"]
ab = grocery.copy()

## Insert elements in a list
print("Before: {}".format(ab))

# ab.append("Maggie")
# ab.append("Butter")
# ab.insert(2,"Cherry")

## Remove elements
# ab.pop()
# v = ab.pop(2)
# print(v,"removed successfully!!")
# ab.remove("Bread")
# del ab[1]

## Update value
# ab[1] = "Tomato"
print(dir(ab))

# print(ab.count("Milk"))
# print(ab.count("Mustrad Oil"))
# ab.reverse()
# ab.clear()
# print(ab.index("Milk",2))

print("After: {}".format(ab))
