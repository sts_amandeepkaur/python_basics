# Syntax of lambda
# lambda p1,p2:p1+p2

rs = lambda a,b:a+b
print(rs(10,29))

ab = lambda c:c**3
print(ab(20))

# def add(a,b):
#     return a+b

# rs = add(10,20)
# print(rs)