class MyError(Exception):
    def __init__(self):
        self.code = "Error 101: Please Login First!!!"
    def __str__(self):
        return self.code

try:
    print("HELLO THERE")
    raise MyError

except MyError as a:
    print(a)
    print("Something Wrong!!!")


