marks = [87,55,2,59,1,0,98,13,76]
str = "jdwsCj6hjewh92nj4A732hBbd3y87h"
# Syntax
#     filter(fun, *sequence)

# def abcd(x):
#     if x>=33:
#         return x

# rs = list(filter(abcd,marks))

rs = list(filter(lambda x:x>=33,marks))
rs = list(filter(lambda x:x<33,marks))
print(rs)

d= list(filter(lambda x:x.isdigit(),str))
d= list(filter(lambda x:x.isupper(),str))
print(d)

# ls= []
# for i in marks:
#     if i>=33:
#         ls.append(i)

# print(ls)