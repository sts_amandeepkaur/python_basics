import threading 
import time

def myfun1():
    for i in range(5):
        time.sleep(1)
        print("Task 1")

def myfun2(a,b):
    for i in range(5):
        time.sleep(1)
        print("Task 2: {} + {} = {}".format(a,b,a+b))

    
t1 = threading.Thread(target=myfun1)
t2 = threading.Thread(target=myfun2,args=(10,5))

t1.start()
t2.start()

t1.join()
t2.join()

print("I WANT TO EXECUTE AT LAST")
