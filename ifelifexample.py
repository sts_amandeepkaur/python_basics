st = """
    Press 1: To add Two given numbers
    Press 2: To subtract Two given numbers
    Press 3: To multiply Two given numbers
    Press 4: To divide Two given numbers
    Press 0:To Exit
******************************************

"""
print(st)
while True:
    num1 = int(input("Enter Number: "))
    num2 = int(input("Enter Number: "))

    choice = input("Enter Your Choice: ")
    if choice == "1":
        result = num1+num2
        print("{} + {} = {}".format(num1,num2,result))

    elif choice=="2":
        result = num1-num2
        print("{} - {} = {}".format(num1,num2,result))

    elif choice=="3":
        result = num1*num2
        print("{} * {} = {}".format(num1,num2,result))

    elif choice=="4":
        result = num1/num2
        print("{} / {} = {}".format(num1,num2,result))
    
    elif choice=="0":
        break
        
    else:
        print("INCORRECT OPTION!!!")