from threading import Thread 

class MyClass1(Thread):
    def run(self):
        for i in range(5):
            print("Hello")

class MyClass2(Thread):
    def run(self):
        for i in range(5):
            print("Hii")

t1=MyClass1()
t2=MyClass2()

t1.start()
t2.start()

t1.join()
t2.join()
print("HELLO I AM MAIN THREAD")