def student(name,email,roll_no):
    print(" Name:{}\n Email: {}\n Roll No: {}".format(name,email,roll_no))


### Positional Arguments
# student("Amandeep",1001,"aman@gmail.com")
# student("Peter","peter@gmail.com",1002)

student(name="Amandeep Kaur",roll_no=1001,email="akd6203@gmail.com")
student("Shinchan",email="s@gmail.com",roll_no=1001)
student("James","s@gmail.com",roll_no=1001)

# def add(x,y):
#     return x+y

# print(add(10,20))

