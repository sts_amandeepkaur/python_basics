student = {
    "name":"Amandeep Kaur",
    "roll_no":1001,
    "fee":1000.00,
    "subjects":("Python","PHP","Java"),
    "hobbies":["Reading","Travelling"],
    "is_present":True,
    "fail_subjects":None,
    "Technology":{
        "front-end":["HTML","CSS","JS"],
        "back-end":["Python","Django","SQLite"]
    }
}
print(type(student))
# print(student)

##Access elements
print(student["name"])
print(student["roll_no"])
print(student["subjects"][2])
print(student["Technology"]["back-end"][1])

## Iterate to a dictionary

for i in student:
    print(i,"=>",student[i])