ab = {
    "Product_name":"Samsung Mobile",
    "price":10000,
    "category":"Electronics",
    "added_on":"24-April-2020",
}

# print(type(ab))
# print(dir(ab))

## Dictionary methods
print(ab.keys())

product = ab.copy()

print(product)
# c=1
# for i in product.values():
#     print(c,":",i)
#     c+=1

# print(product.items())
# for k,v in product.items():
#     print("%-15s : %s"%(k,v))
#     # print("{} : {}".format(k,v))

### Get element
print(product["price"])
# print(product.get("image"))

### Insert element
product["sale_price"]=500

## Update element
product["price"] = 1000
product.update({"price":2000,"seller":"Samsung"})
print(product)

## Remove elements
# item=product.pop("seller")
# print(item,"item removed")
# product.popitem()
del product["sale_price"]

### Delete dictionary
del product
print(ab)