a = 'Hello World'

print(type(a))

## Singleline comment
# number can be integer , float, complex
# class student
# Comments are used to add documentation in program. Shortcut VSCode  ctrl+/

'''
Multi-line comment
Python data Types
int
string
tuple
dictionary
'''
print(a)
b = 10