class Student:
    def intro(self,name,roll_no): #a,b
        self.n = name
        # print("Name: ",name)
        # print("Roll_No: ",roll_no)


class Account(Student): #c,d
    def fee_details(self,total,pending):
        print(self.n,"has paid Rs. {}/-".format(total-pending))
        print("Total Fee: Rs.{}/-".format(total))
        print("Pending Fee: Rs.{}/-".format(pending))


class Library(Student): #e,f
    def issue_book(self,bn,isdt,rtdt):
        print(self.n," Book Details: ")
        print("Book Name: {}".format(bn))
        print("Issue Date: {}".format(isdt))
        print("Return Date: {}".format(rtdt))

st1 = Library()
st1.intro("Amandeep Kaur",20001)
st1.issue_book("Python Programming","10-May-2020","28-May-2020")

# acc = Account()
# acc.intro("Aman",10001)

# ob2 = Account()
# ob2.intro("Harry Potter",10002)

# acc.fee_details(10000,500)


