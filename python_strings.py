a = "It is a python string"
b = "It is"
# print(a)

##Indexing - Python allows both forward and reverse indexing

# print(a[0])
# print(a[1])

# print(len(b))
# print(b[4])
# print(a[-1])
# print(a[-4])

## Slicing
ab = "My name is Amandeep kaur"
ed = "0123456789"
# print(ab[4:])
# print(ab[:4])
# print(ab[-3:])
# print(ab[:-3])
# print(ab[3:10])

## Step Size
# print(ab[::1])
# print(ab[::2])
# print(ed[::2])
# print(ed[::3])
# print(ed[::-1])
# print(ab[::-1])

## String oprators 
## Membership -- in, not in
# ab = "Python is very easy language"
# if "Aman" in ab:
#     print("Element Found")
# else:
#     print("Not Found!!!")

##Concatenation
str1 = "Hello"
str_two = "There"
# num = "10"

# print(str1+str_two)
# print(str1+num)

## Identity operators - is, is not
print(str1 is str1)
print(str1 is str_two)