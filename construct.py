class Circle:
    def __init__(self):
        self. a = 10
        print("I am a constructor function")
        print(self.a)
        # return "End of functon" ##Constructor should return none

    def abcd(self):
        print("I am a member function")


    def __del__(self):
        del self.a 
        ### print(self.a)
        print("Destructor called , Values Destroyed!!!")
    

obj = Circle()
