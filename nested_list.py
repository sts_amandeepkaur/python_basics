ab = [
    [10,20,49],
    [86,6,33],
    [55,64,77]
]

### Transpose of a matrix
result = [[0,0,0],[0,0,0],[0,0,0]]
for i in range(len(ab)):
    for j in range(len(ab[i])):
        result[i][j] = ab[j][i]
        # print("ab[{}][{}]={}".format(i,j,ab[i][j]))

for i in ab:
    print(i)
print("--------------------------")
for r in result:
    print(r)