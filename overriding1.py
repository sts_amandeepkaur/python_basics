class Parent:
    a="Hello"
    b=[10,4,6]
    def info(self):
        print("Parent Member Function")

class Child(Parent):
    a,b="There","Python"
    def info(self):
        print("Child Member Function")

obj = Child()
print(obj.a)
print(obj.b)
obj.info()
