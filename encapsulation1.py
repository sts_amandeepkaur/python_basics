class Student:
    #Public Data member
    clz = "XYZ Engg. College"

    #Private Data member
    __pincode=100005432

    #Private Member Function
    def __intro(self):
        print("Member Function: ",self.__pincode)

    def __init__(self):
        print("Initialized")
        self.__intro()


obj = Student()
## obj.__intro() Object can't access private M.F. of a class
# print(obj.__pincode) #Object can't access private D.M. of a class
# print(obj.clz)
# obj.clz="ABC College of Management"
# print(obj.clz)
# obj.intro()