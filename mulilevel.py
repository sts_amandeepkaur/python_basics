class GrandParent: #a,b
    def myfun1(self):
        print("It is a function of Grand Parent")

class Parent(GrandParent): #c,d
    def myfun2(self):
        print("It is a function of Parent Class")

class Child(Parent): #e,f
    def myfun3(self):
        print("It is a function of child class")

obj1 = Child()
obj1.myfun3()
obj1.myfun2()
obj1.myfun1()

# obj2 = Parent()
# obj2.myfun2()
# obj2.myfun1()

# print(dir(obj1))