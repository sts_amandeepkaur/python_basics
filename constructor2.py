class Student:
    def __init__(self,name,email,is_present=False):
        self.n = name
        self.e = email
        self.status = is_present

    def get_data(self):
        print("Name: %-15s"%(self.n))
        print("Email: %-15s"%(self.e))
        if self.status:
            print("Attendance: Present")
        else:
            print("Attendance: Absent")
        print("______________________________\n")

    def mark_attendance(self):
        self.status = True
        print(self.n,"'s Attendance Marked")

    def __del__(self):
        del self.n
        del self.e
        del self.status
        print("All resourses are free which was allocated")

st1 = Student("Peter","peter@gmail.com")
st2 = Student("shinchan","s@gmail.com")
st3 = Student("James","james@gmail.com")

st2.mark_attendance()
st3.mark_attendance()

st2.get_data()
st1.get_data()
st3.get_data()

