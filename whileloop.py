# num = 1
# while num<=10:
#     print(num)
#     num = num+1

## Access list element using while loop

ls = ["red","green","blue","purple","black","magenta"]

total = len(ls)
index = 0
while index < total:
    if ls[index] == "purple" or ls[index] == "magenta" :
        print("I don't like {} color".format(ls[index]))
    else:
        print("Color is: ",ls[index])
    index += 1